#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H
#include <QHBoxLayout>
#include <QMainWindow>
#include <QImage>
#ifndef QT_NO_PRINTER
#include <QPrinter>
#endif

class QAction;
class QLabel;
class QMenu;
class QScrollArea;
class QScrollBar;

class ImageViewer : public QMainWindow
{
    Q_OBJECT

public:
    ImageViewer();
    bool loadFile(const QString &);

private slots:
    void    open();
    void    saveAs();
    void    copy();
    void    paste();
    void    zoomIn();
    void    zoomOut();
    void    normalSize();
    void    fitToWindow();
    void    about();

private:
    void    createActions();
    void    createMenus();
    void    updateActions();
    bool    saveFile(const QString &fileName);
    void    setImage(const QImage &newImage);
    void    scaleImage(double factor);

    QImage  flipHorizontal(QImage imgOriginalImage);
    QImage  flipVertical(QImage imgOriginalImage);
    QImage  setGrayScale(QImage imgOriginalImage,  bool bQuantum);
    QColor  getGrayPixel(QColor colorPixel, bool bQuantum);
    float   getGrayTone(QColor colorPixel);

    void    resetImage();
    void    transformHorizontal();
    void    transformVertical();
    void    transformGrayScale();
    void    transformQuantumGrayScale();

    void    adjustScrollBar(QScrollBar *scrollBar, double factor);

    QImage  image;
    QLabel  *imageLabelOriginal;
    QLabel  *imageLabelNew;
    QScrollArea *scrollAreaOriginal;
    QScrollArea *scrollAreaNew;
    double  scaleFactor;

#ifndef QT_NO_PRINTER
    QPrinter printer;
#endif

    QAction *saveAsAct;
    QAction *copyAct;
    QAction *zoomInAct;
    QAction *zoomOutAct;
    QAction *normalSizeAct;
    QAction *fitToWindowAct;
    QAction *flipVerticalAct;
    QAction *flipHorizontalAct;
    QAction *setGrayScaleAct;
    QAction *QuantumGrayScaleAct;
    QAction *resetImageAct;
};

#endif

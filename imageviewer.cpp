#include <QtWidgets>
#if defined(QT_PRINTSUPPORT_LIB)
#include <QtPrintSupport/qtprintsupportglobal.h>
#if QT_CONFIG(printdialog)
#include <QPrintDialog>
#include "globals.h"
#endif
#endif

#include "imageviewer.h"

ImageViewer::ImageViewer()
   : imageLabelOriginal(new QLabel)
   , imageLabelNew(new QLabel)
   , scrollAreaOriginal(new QScrollArea)
   , scrollAreaNew(new QScrollArea)
   , scaleFactor(1)
{
    imageLabelOriginal->setBackgroundRole(QPalette::Base);
    imageLabelOriginal->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabelOriginal->setScaledContents(true);

    imageLabelNew->setBackgroundRole(QPalette::Base);
    imageLabelNew->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabelNew->setScaledContents(true);

    scrollAreaOriginal->setBackgroundRole(QPalette::Dark);
    scrollAreaOriginal->setWidget(imageLabelOriginal);
    scrollAreaOriginal->setVisible(false);

    scrollAreaNew->setBackgroundRole(QPalette::Dark);
    scrollAreaNew->setWidget(imageLabelNew);
    scrollAreaNew->setVisible(false);

    // Do the layout
    QWidget *centralWidget = new QWidget;
    QHBoxLayout *layout = new QHBoxLayout(centralWidget);
    layout->addWidget(scrollAreaOriginal);
    layout->addWidget(scrollAreaNew);

    setCentralWidget(centralWidget);

    createActions();

    resize(QGuiApplication::primaryScreen()->availableSize() * 3 / 5);
}


bool ImageViewer::loadFile(const QString &fileName)
{
    QImageReader reader(fileName);
    reader.setAutoTransform(true);
    const QImage newImage = reader.read();
    if (newImage.isNull()) {
        QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
                                 tr("Cannot load %1: %2")
                                 .arg(QDir::toNativeSeparators(fileName), reader.errorString()));
        return false;
    }

    originalImage = newImage;
    setImage(newImage);

    setWindowFilePath(fileName);

    const QString message = tr("Opened \"%1\", %2x%3, Depth: %4")
        .arg(QDir::toNativeSeparators(fileName)).arg(image.width()).arg(image.height()).arg(image.depth());
    statusBar()->showMessage(message);
    return true;
}

    void ImageViewer::resetImage(){
        setImage(originalImage);
    }

    void ImageViewer::transformHorizontal(){
        QImage newImage = flipHorizontal(currentImage); 
        setImage(newImage);
    }

    void ImageViewer::transformVertical(){
        QImage newImage = flipVertical(currentImage); 
        setImage(newImage);
    }

    void ImageViewer::transformGrayScale(){
        QImage newImage = setGrayScale(currentImage, false); 
        setImage(newImage);
    }

     void ImageViewer::transformQuantumGrayScale(){
        QImage newImage = setGrayScale(currentImage, true); 
        setImage(newImage);
    }



QImage ImageViewer::setGrayScale(QImage imgOriginalImage, bool bQuantum){
    QImage imgGrayScaleImage = imgOriginalImage;
    int width = imgGrayScaleImage.size().width();
    int height = imgGrayScaleImage.size().height();

    for(int x =0; x<width; x++){
        for (int y =0; y<height; y++){
            
            QColor colorGrayPixel = getGrayPixel(imgGrayScaleImage.pixelColor(x, y), bQuantum);
            
            imgGrayScaleImage.setPixelColor(x, y, colorGrayPixel);
        }
    }
    return imgGrayScaleImage;
}

QColor ImageViewer::getGrayPixel(QColor colorPixel, bool bQuantum){
    int iGrayTone;
    if (bQuantum){
        float fGrayTone = getGrayTone(colorPixel);
        float fQuantum = 128;
        fGrayTone = fmod(fGrayTone, fQuantum);
        iGrayTone = int(fGrayTone + 0.5);
        int divisor = 256/fQuantum;
        iGrayTone = iGrayTone*divisor;
    } else {
        iGrayTone = getGrayTone(colorPixel);
    }
    QColor colorGrayPixel = colorPixel; 
    colorGrayPixel.setRed(iGrayTone);
    colorGrayPixel.setBlue(iGrayTone);
    colorGrayPixel.setGreen(iGrayTone);

    return colorGrayPixel;
}

float ImageViewer::getGrayTone(QColor colorPixel){
    int iGrayTone = 
          0.299*colorPixel.red() 
        + 0.587*colorPixel.green() 
        + 0.114*colorPixel.blue();
    return iGrayTone;
}

QImage ImageViewer::flipHorizontal(QImage imgOriginalImage){

    QImage imgFlippedHorizontally = imgOriginalImage;
    int width = imgFlippedHorizontally.size().width();
    int height = imgFlippedHorizontally.size().height();

    for(int x = 0; x<width; x++){
        for (int y = 0; y<height; y++){
            QColor colorOriginalPixel = imgOriginalImage.pixelColor(x, y);
            int iNewX = width - x - 1;
            imgFlippedHorizontally.setPixelColor(iNewX, y, colorOriginalPixel);
        }
    }

    return imgFlippedHorizontally;
}

QImage ImageViewer::flipVertical(QImage imgOriginalImage){

    QImage imgFlippedVertically = imgOriginalImage;
    int width = imgFlippedVertically.size().width();
    int height = imgFlippedVertically.size().height();

    for(int x = 0; x<width; x++){
        for (int y = 0; y<height; y++){
            QColor colorOriginalPixel = imgOriginalImage.pixelColor(x, y);
            int iNewY = height - y -1;
            imgFlippedVertically.setPixelColor(x, iNewY, colorOriginalPixel);
        }
    }

    return imgFlippedVertically;
}

void ImageViewer::setImage(const QImage &newImage)
{
    image = newImage;
    imageLabelOriginal->setPixmap(QPixmap::fromImage(originalImage));
    imageLabelNew->setPixmap(QPixmap::fromImage(newImage));
    scaleFactor = 1;

    scrollAreaOriginal->setVisible(true);
    scrollAreaNew->setVisible(true);
    fitToWindowAct->setEnabled(true);
    currentImage = newImage;
    updateActions();

    if (!fitToWindowAct->isChecked())
    {
        imageLabelOriginal->adjustSize();
        imageLabelNew->adjustSize();
    }
}


bool ImageViewer::saveFile(const QString &fileName)
{
    QImageWriter writer(fileName);

    if (!writer.write(image)) {
        QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
                                 tr("Cannot write %1: %2")
                                 .arg(QDir::toNativeSeparators(fileName)), writer.errorString());
        return false;
    }
    const QString message = tr("Wrote \"%1\"").arg(QDir::toNativeSeparators(fileName));
    statusBar()->showMessage(message);
    return true;
}


static void initializeImageFileDialog(QFileDialog &dialog, QFileDialog::AcceptMode acceptMode)
{
    static bool firstDialog = true;

    if (firstDialog) {
        firstDialog = false;
        const QStringList picturesLocations = QStandardPaths::standardLocations(QStandardPaths::PicturesLocation);
        dialog.setDirectory(picturesLocations.isEmpty() ? QDir::currentPath() : picturesLocations.last());
    }

    QStringList mimeTypeFilters;
    const QByteArrayList supportedMimeTypes = acceptMode == QFileDialog::AcceptOpen
        ? QImageReader::supportedMimeTypes() : QImageWriter::supportedMimeTypes();
    foreach (const QByteArray &mimeTypeName, supportedMimeTypes)
        mimeTypeFilters.append(mimeTypeName);
    mimeTypeFilters.sort();
    dialog.setMimeTypeFilters(mimeTypeFilters);
    dialog.selectMimeTypeFilter("image/jpeg");
    if (acceptMode == QFileDialog::AcceptSave)
        dialog.setDefaultSuffix("jpg");
}

void ImageViewer::open()
{
    QFileDialog dialog(this, tr("Open File"));
    initializeImageFileDialog(dialog, QFileDialog::AcceptOpen);

    while (dialog.exec() == QDialog::Accepted && !loadFile(dialog.selectedFiles().first())) {}
}

void ImageViewer::saveAs()
{
    QFileDialog dialog(this, tr("Save File As"));
    initializeImageFileDialog(dialog, QFileDialog::AcceptSave);

    while (dialog.exec() == QDialog::Accepted && !saveFile(dialog.selectedFiles().first())) {}
}

void ImageViewer::copy()
{
#ifndef QT_NO_CLIPBOARD
    QGuiApplication::clipboard()->setImage(image);
#endif // !QT_NO_CLIPBOARD
}

#ifndef QT_NO_CLIPBOARD
static QImage clipboardImage()
{
    if (const QMimeData *mimeData = QGuiApplication::clipboard()->mimeData()) {
        if (mimeData->hasImage()) {
            const QImage image = qvariant_cast<QImage>(mimeData->imageData());
            if (!image.isNull())
                return image;
        }
    }
    return QImage();
}
#endif // !QT_NO_CLIPBOARD

void ImageViewer::paste()
{
#ifndef QT_NO_CLIPBOARD
    const QImage newImage = clipboardImage();
    if (newImage.isNull()) {
        statusBar()->showMessage(tr("No image in clipboard"));
    } else {
        originalImage = newImage;
        setImage(newImage);
        setWindowFilePath(QString());
        const QString message = tr("Obtained image from clipboard, %1x%2, Depth: %3")
            .arg(newImage.width()).arg(newImage.height()).arg(newImage.depth());
        statusBar()->showMessage(message);
    }
#endif // !QT_NO_CLIPBOARD
}

void ImageViewer::zoomIn()
{
    scaleImage(1.25);
}

void ImageViewer::zoomOut()
{
    scaleImage(0.8);
}

void ImageViewer::normalSize()
{
    imageLabelOriginal->adjustSize();
    scaleFactor = 1.0;
}

void ImageViewer::fitToWindow()
{
    bool fitToWindow = fitToWindowAct->isChecked();
    scrollAreaOriginal->setWidgetResizable(fitToWindow);
    if (!fitToWindow)
        normalSize();
    updateActions();
}

void ImageViewer::about()
{
    QMessageBox::about(this, tr("About Image Viewer"),
            tr("<p>The <b>Image Viewer</b> example shows how to combine QLabel "
               "and QscrollAreaOriginal to display an image. QLabel is typically used "
               "for displaying a text, but it can also display an image. "
               "QscrollAreaOriginal provides a scrolling view around another widget. "
               "If the child widget exceeds the size of the frame, QscrollAreaOriginal "
               "automatically provides scroll bars. </p><p>The example "
               "demonstrates how QLabel's ability to scale its contents "
               "(QLabel::scaledContents), and QscrollAreaOriginal's ability to "
               "automatically resize its contents "
               "(QscrollAreaOriginal::widgetResizable), can be used to implement "
               "zooming and scaling features. </p><p>In addition the example "
               "shows how to use QPainter to print an image.</p>"));
}

void ImageViewer::createActions()
{
    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));

    QAction *openAct = fileMenu->addAction(tr("&Open..."), this, &ImageViewer::open);
    openAct->setShortcut(QKeySequence::Open);

    saveAsAct = fileMenu->addAction(tr("&Save As..."), this, &ImageViewer::saveAs);
    saveAsAct->setEnabled(false);

    fileMenu->addSeparator();

    QAction *exitAct = fileMenu->addAction(tr("E&xit"), this, &QWidget::close);
    exitAct->setShortcut(tr("Ctrl+Q"));

    QMenu *editMenu = menuBar()->addMenu(tr("&Edit"));

    copyAct = editMenu->addAction(tr("&Copy"), this, &ImageViewer::copy);
    copyAct->setShortcut(QKeySequence::Copy);
    copyAct->setEnabled(false);

    QAction *pasteAct = editMenu->addAction(tr("&Paste"), this, &ImageViewer::paste);
    pasteAct->setShortcut(QKeySequence::Paste);

    flipVerticalAct = editMenu->addAction(tr("Flip Ve&rtical"), this, &ImageViewer::transformVertical);
    flipVerticalAct->setEnabled(false);
    flipVerticalAct->setShortcut(tr("Ctrl+R"));

    flipHorizontalAct = editMenu->addAction(tr("Flip &Horizontal"), this, &ImageViewer::transformHorizontal);
    flipHorizontalAct->setEnabled(false);
    flipHorizontalAct->setShortcut(tr("Ctrl+H"));

    setGrayScaleAct = editMenu->addAction(tr("&Gray Scale"), this, &ImageViewer::transformGrayScale);
    setGrayScaleAct->setEnabled(false);
    setGrayScaleAct->setShortcut(tr("Ctrl+G"));

    QuantumGrayScaleAct = editMenu->addAction(tr("Q&uantum Gray Scale"), this, &ImageViewer::transformQuantumGrayScale);
    QuantumGrayScaleAct->setEnabled(false);
    QuantumGrayScaleAct->setShortcut(tr("Ctrl+Y"));

    resetImageAct = editMenu->addAction(tr("R&eset Image"), this, &ImageViewer::resetImage);
    resetImageAct->setEnabled(false);
    resetImageAct->setShortcut(tr("Ctrl+E"));

    QMenu *viewMenu = menuBar()->addMenu(tr("&View"));

    zoomInAct = viewMenu->addAction(tr("Zoom &In (25%)"), this, &ImageViewer::zoomIn);
    zoomInAct->setShortcut(QKeySequence::ZoomIn);
    zoomInAct->setEnabled(false);

    zoomOutAct = viewMenu->addAction(tr("Zoom &Out (25%)"), this, &ImageViewer::zoomOut);
    zoomOutAct->setShortcut(QKeySequence::ZoomOut);
    zoomOutAct->setEnabled(false);

    normalSizeAct = viewMenu->addAction(tr("&Normal Size"), this, &ImageViewer::normalSize);
    normalSizeAct->setShortcut(tr("Ctrl+S"));
    normalSizeAct->setEnabled(false);

    viewMenu->addSeparator();

    fitToWindowAct = viewMenu->addAction(tr("&Fit to Window"), this, &ImageViewer::fitToWindow);
    fitToWindowAct->setEnabled(false);
    fitToWindowAct->setCheckable(true);
    fitToWindowAct->setShortcut(tr("Ctrl+F"));

    QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));

    helpMenu->addAction(tr("&About"), this, &ImageViewer::about);
    helpMenu->addAction(tr("About &Qt"), &QApplication::aboutQt);
}

void ImageViewer::updateActions()
{
    bool isImageNull = !image.isNull();

    QuantumGrayScaleAct->   setEnabled(isImageNull);
    flipVerticalAct->       setEnabled(isImageNull);
    flipHorizontalAct->     setEnabled(isImageNull);
    setGrayScaleAct->       setEnabled(isImageNull);
    resetImageAct->         setEnabled(isImageNull);
    saveAsAct->             setEnabled(isImageNull);
    copyAct->               setEnabled(isImageNull);
    zoomInAct->             setEnabled(!fitToWindowAct->isChecked());
    zoomOutAct->            setEnabled(!fitToWindowAct->isChecked());
    normalSizeAct->         setEnabled(!fitToWindowAct->isChecked());
}

void ImageViewer::scaleImage(double factor)
{
    Q_ASSERT(imageLabelOriginal->pixmap());
    scaleFactor *= factor;
    imageLabelOriginal->resize(scaleFactor * imageLabelOriginal->pixmap()->size());
    imageLabelNew->resize(scaleFactor * imageLabelOriginal->pixmap()->size());

    adjustScrollBar(scrollAreaOriginal->horizontalScrollBar(), factor);
    adjustScrollBar(scrollAreaOriginal->verticalScrollBar(), factor);

    adjustScrollBar(scrollAreaNew->horizontalScrollBar(), factor);
    adjustScrollBar(scrollAreaNew->verticalScrollBar(), factor);

    zoomInAct->setEnabled(scaleFactor < 3.0);
    zoomOutAct->setEnabled(scaleFactor > 0.333);
}

void ImageViewer::adjustScrollBar(QScrollBar *scrollBar, double factor)
{
    scrollBar->setValue(int(factor * scrollBar->value()
                            + ((factor - 1) * scrollBar->pageStep()/2)));
}

QT += widgets
qtHaveModule(printsupport): QT += printsupport
QT += core

HEADERS       = imageviewer.h \
    globals.h
SOURCES       = imageviewer.cpp \
                main.cpp \
    globals.cpp

# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/widgets/imageviewer
INSTALLS += target

DISTFILES += \
    UiForm.ui.qml \
    Ui.qml
